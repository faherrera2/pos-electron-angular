import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import {DecimalPipe} from '@angular/common';
import {Observable} from 'rxjs';

import {NgbdSortableHeader, SortEvent} from './../shared/directives/datatable/sortable.directive';

import { ElectronService } from './../core/services'
import { totalmem } from 'os';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [DecimalPipe]
})
export class HomeComponent implements OnInit {
  /*
  countries$: Observable<Country[]>;
  total$: Observable<number>;
  
  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;
  */

  pedido = [];

 productos = [];
 datosCliente = [];

  filtro = {
    txt: ""
  }

  filtroCliente = "";

  constructor(public electronservice: ElectronService) { 
    //this.countries$ = service.countries$;
    //this.total$ = service.total$;
   }

  recalcular(p){
    p.total = parseFloat((p.cantidad * p.valor_iva).toFixed(2));
  }

  agregar(p){
    this.pedido.push(p);
  }

  buscar(){
    this.electronservice.getPorNombre(this.filtro.txt).then((data: any) => {
      this.productos = data;
      this.productos.forEach(p => {
        p.cantidad = 1;
        p.decuento = 0.00;
        p.subtotal = 0.00;
        p.total = p.valor_iva * p.cantidad;
      });
      console.log("los productos:", data[0]);
    })
  }


  buscarcliente(){
    this.electronservice.getCliente(this.filtroCliente).then((cliente: any) => {
      this.datosCliente = cliente      
      console.log(this.datosCliente);
    })
  }
   
  facturar(){
    console.log(this.pedido);
    let totatlFactura = 0.00;
    this.pedido.forEach(p => {
      totatlFactura += p.total;
    });
    totatlFactura = parseFloat(totatlFactura.toFixed(2));
    console.log("El total ",totatlFactura);
  }

  ngOnInit() {
    this.electronservice.getProducts().then( (products: Array<any>) => {
      this.productos = products;
      this.productos.forEach(p => {
        p.cantidad = 1;
        p.decuento = 0.00;
        p.subtotal = 0.00;
        p.total = parseFloat((p.valor_iva * p.cantidad).toFixed(2));
      });
      
    })

  }

}
