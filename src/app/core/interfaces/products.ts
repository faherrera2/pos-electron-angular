export interface Products {
    nombre: string,
    ci_ruc: string,
    direccion: string,
    telefono: string,
    correo: string
}
