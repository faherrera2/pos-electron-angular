import { app, BrowserWindow, screen, ipcMain } from 'electron';
import * as path from 'path';
import * as url from 'url';

let win, serve;
const args = process.argv.slice(1);
serve = args.some(val => val === '--serve');

function createWindow() {

  const electronScreen = screen;
  const size = electronScreen.getPrimaryDisplay().workAreaSize;

  // Create the browser window.
  win = new BrowserWindow({
    x: 0,
    y: 0,
    width: size.width,
    height: size.height,
    webPreferences: {
      nodeIntegration: true,
    },
  });

  if (serve) {
    require('electron-reload')(__dirname, {
      electron: require(`${__dirname}/node_modules/electron`)
    });
    win.loadURL('http://localhost:4200');
  } else {
    win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
  }

  if (serve) {
    win.webContents.openDevTools();
    ipcMain.on('datos-prueba', (evento, dato) => {
      getAllProducts()
        .then(products => {
          win.webContents.send('products', products);
          console.log(products)
        })
        win.webContents.send('hola', [{id:1, id2:3}]);
    })

    ipcMain.on('datosBusqueda', (evento, dato) => {
      console.log("el dato: ",dato);
      getProductName(dato)
        .then(products => {
          win.webContents.send('products-name', products);
          console.log(products)
        })    
    });

    ipcMain.on('clienteBusqueda', (evento, cliente) => {
      console.log("el cliente: ",cliente);
      getClient(cliente)
        .then(cliente => {
          win.webContents.send('cliente-name', cliente);
          console.log(cliente)
        })
        
    })
    
  }

  // Emitted when the window is closed.
  win.on('closed', () => {
    // Dereference the window object, usually you would store window
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    win = null;
  });

}

try {

  // This method will be called when Electron has finished
  // initialization and is ready to create browser windows.
  // Some APIs can only be used after this event occurs.
  app.on('ready', createWindow);

  // Quit when all windows are closed.
  app.on('window-all-closed', () => {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
      app.quit();
    }
  });

  app.on('activate', () => {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (win === null) {
      createWindow();
    }
  });

} catch (e) {
  // Catch Error
  // throw e;
}

const getAllProducts = () => {
  var sqlite3 = require('sqlite3').verbose();
  var db = new sqlite3.Database('pos');

  return new Promise((resolve, reject) => {
    db.serialize(function () {
      db.run("CREATE TABLE IF NOT EXISTS productos (id_prod INTEGER PRIMARY KEY, codigo TEXT, nombre TEXT, descripcion TEXT)");
      db.run("CREATE TABLE IF NOT EXISTS precios (id_precio INTEGER, tipo_precio TEXT, valor FLOAT, valor_iva FLOAT, id_prod INTEGER, FOREIGN KEY(id_prod) REFERENCES productos (id_prod))");
      db.run("CREATE TABLE IF NOT EXISTS stock (id_stock INTEGER, bodega TEXT, qty INTEGER, id_prod INTEGER, FOREIGN KEY(id_prod) REFERENCES productos (id_prod))");
    });
    db.all('SELECT pro.id_prod, pro.codigo, pro.descripcion, pre.valor, pre.valor_iva FROM productos pro LEFT JOIN precios pre ON pro.id_prod = pre.id_prod AND pre.tipo_precio = "p_a"', function (err, rows) {
      resolve(rows)
    });
  })
}

const getProductId = (idProduct) => {
  var sqlite3 = require('sqlite3').verbose();
  var db = new sqlite3.Database('pos');
  // send to the Main Window
  console.log('===============================')
  console.log(idProduct);
  return new Promise((resolve, reject) => {
    db.all(`SELECT pro.id_prod, pro.codigo, pro.descripcion, pre.valor, pre.valor_iva FROM productos pro LEFT JOIN precios pre ON pro.id_prod = pre.id_prod AND pre.tipo_precio = "p_a" WHERE pro.id_prod = ${idProduct}`, function (err, row) {
      resolve(row)
    });
  })

  

};


const getProductName = (name) => {
  var sqlite3 = require('sqlite3').verbose();
  var db = new sqlite3.Database('pos');
  // send to the Main Window
  console.log('===============================')
  return new Promise((resolve, reject) => {
    db.all("SELECT pro.id_prod, pro.codigo, pro.descripcion, pre.valor, pre.valor_iva FROM productos pro LEFT JOIN precios pre ON pro.id_prod = pre.id_prod AND pre.tipo_precio = 'p_a' WHERE pro.descripcion LIKE \'%"+name+"%\' OR pro.codigo LIKE \'%"+name+"%\' " ,function (err, row) {
      console.log(row);
      resolve(row)
    // db.all('SELECT pro.id_prod, pro.codigo, pro.descripcion, pre.valor, pre.valor_iva FROM productos pro LEFT JOIN precios pre ON pro.id_prod = pre.id_prod AND pre.tipo_precio = "p_a"', function (err, rows) {
    //   resolve(rows)
    });
  })
};

const getClient = (client) => {
  var sqlite3 = require('sqlite3').verbose();
  var db = new sqlite3.Database('pos');
  return new Promise((resolve, reject) => {
    db.all("SELECT * FROM clientes WHERE nombres LIKE \'%"+client+"%\' OR ci_ruc LIKE \'%"+client+"%\' " ,function (err, row) {
      console.log(row);
      resolve(row)
    // db.all('SELECT pro.id_prod, pro.codigo, pro.descripcion, pre.valor, pre.valor_iva FROM productos pro LEFT JOIN precios pre ON pro.id_prod = pre.id_prod AND pre.tipo_precio = "p_a"', function (err, rows) {
    //   resolve(rows)
    });
  })
};